/**
 *  An exception that occurs when an LL(1) parse table definition file cannot be parsed.
 */
public class InvalidParseTableException extends Exception
{
    public InvalidParseTableException()
    {
        
    }

    public InvalidParseTableException(String message)
    {
        super(message);
    }

    public InvalidParseTableException(Throwable cause)
    {
        super(cause);
    }

    public InvalidParseTableException(String message, Throwable cause)
    {
        super(message, cause);
    }
}