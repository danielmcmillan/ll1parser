/*
 * Parser.java
 * Author: Daniel McMillan
*/

import java.util.*;
import java.io.*;

/**
 * Class to parse the contents of a file using a parse table
*/
public class Parser
{
    private ParseTable table;
    private String file;
    private boolean recoverFromError;
    
    // An object that handles events that occur during parsing
    private ParseEventHandler handler = null;
    
    // The current stack
    private Deque<String> stack;
    
    // Stack for retaining information about errors
    private Deque<String> retainStack;
    
    // Object for extracting tokens from the input string
    private Tokeniser tok;
    
    // The number of errors that have been encountered while parsing
    private int errorCount;
    
    // Whether the parser is in the process of recovering from an error,
    // in this case subsequent errors will be dropped
    private boolean recovering;
    // Whether a variable with no null-production has been popped during recovery
    private boolean recoveryVarPopped;
    // Whether recovery by popping has failed
    private boolean recoveryFailed;
    
    /**
     * Creates a new Parser
     *
     * @param table     the ParseTable to use for LL(1) parsing
     * @param file      the path to the file to parse the contents of
     * @param enableErrorRecovery whether parsing should continue after an error is encountered
    */
    public Parser(ParseTable table, String file, boolean enableErrorRecovery)
        throws FileNotFoundException
    {
        this.table = table;
        this.file = file;
        recoverFromError = enableErrorRecovery;
    }
    
    /**
     * Sets the event handler
     *
     * @param handler   instance of class implementing ParseEventHandler
    */
    public void setEventHandler(ParseEventHandler handler)
    {
        this.handler = handler;
    }
    
    public void parse() throws IOException
    {
        tok = new Tokeniser(file, table.getAllTerminals());
        
        stack = new ArrayDeque<String>();
        retainStack = new ArrayDeque<String>();
        errorCount = 0;
        recovering = false;
        recoveryVarPopped = false;
        recoveryFailed = false;
        
        // Initiate the stack with the start symbol
        stack.addFirst(table.getStartSymbol());
        performedStep(stack);
        
        // Parse the entire file
        while (performDerivationStep());
    }
    
    // Performs a single derivation step based on tokens from the given Tokeniser
    // Returns whether to continue
    private boolean performDerivationStep() throws IOException
    {
        // If the end of the string is expected
        if (stack.isEmpty())
        {
            return handleEmptyStack();
        }
        
        String top = stack.getFirst();
        
        if (table.isVariable(top))
        {
            // Variable is at top of stack
            return handleStackVariable(top);
        }
        else
        {
            // Terminal at top of stack
            return handleStackTerminal(top);
        }
    }
    
    private boolean handleEmptyStack() throws IOException
    {
        String token = tok.peekToken();
        
        if (token == null)
        {
            // The end of the string was expectedly reached
            finished(errorCount);
            return false;
        }
        else
        {
            // Expected the string to end
            return handleError();
        }
    }
    
    // Handles a variable on the top of the stack
    // Returns whether parsing should continue
    private boolean handleStackVariable(String variable) throws IOException
    {
        String token = tok.peekToken();
        
        String[] rule = table.getRule(variable, token);
        
        if (rule == null)
        {
            // No rule can derive the required terminal
            return handleError();
        }
        
        // If the rule is deriving a terminal from the first set, we can discard the retained symbols
        if (token != null && table.getFirstSet(variable).contains(token))
        {
            retainStack.clear();
            
            // Successful derivation, reset error recovery state
            resetErrorRecovery();
        }
        else
        {
            // Rule is trying to derive a symbol in the follow set
            // An error will result if the token doesn't follow variable in the current context
            // In this case, we want to retain information about what was expected at this point
            retainStack.addFirst(variable);
        }
        
        // Pop from the stack
        stack.removeFirst();
        
        // Push the new rule to the stack
        pushRule(rule);
        
        if (!recovering)
        {
            performedStep(stack);
        }

        return true;
    }
    
    // Handles a terminal on the top of the stack
    // Returns whether parsing should continue
    private boolean handleStackTerminal(String terminal) throws IOException
    {
        String token = tok.peekToken();
        
        if (terminal.equals(token))
        {
            // Consume the token and the stack symbol
            tok.consumeToken();
            stack.removeFirst();
            
            retainStack.clear();
        }
        else
        {
            // The token is unexpected
            return handleError();
        }
        
        performedStep(stack);
        resetErrorRecovery();
        return true;
    }
    
    // Pushes the symbols for the specified rule to the stack
    private void pushRule(String[] rule)
    {
        for (int i = rule.length - 1; i >= 0; --i)
        {
            stack.addFirst(rule[i]);
        }
    }
    
    // Handles an error given the top of the stack
    // Returns whether parsing should continue
    private boolean handleError() throws IOException
    {
        String token = tok.peekToken();
        
        restoreStack();
        
        // Don't report subsequent errors
        if (!recovering)
        {
            encounteredError(getFollowSet(), token, tok.getLineNumber());
            ++errorCount;
        }
        
        if (recoverFromError)
        {
            recovering = true;
            
            if (!recoveryFailed && table.getAllTerminals().contains(token))
            {
                // Symbol is valid terminal, assume that it was intended
                
                expandSymbol();
                
                if (stack.size() <= 1)
                {
                    // Popping from stack would cause empty stack, skip token instead
                    recoveryFailed = true;
                    return true;
                }
                
                expandNullProductions();

                String top = stack.getFirst();
                
                if (table.isVariable(top))
                {
                    if (recoveryVarPopped)
                    {
                        // Give up popping from the stack
                        recoveryFailed = true;
                    }
                    else
                    {
                        stack.removeFirst();
                        // This should be the last variable skipped if it doesn't have a null production
                        if (!table.getFirstSet(top).contains(null))
                        {
                            recoveryVarPopped = true;
                        }
                    }
                }
                else
                {
                    stack.removeFirst();
                }
            }
            else
            {
                // Skip the next token
                if (tok.consumeToken() == null)
                {
                    // Got to the end of the file while skipping tokens
                    finished(errorCount);
                    return false;
                }
            }
            return true;
        }
        else
        {
            finished(1);
            return false;
        }
    }
    
    private void resetErrorRecovery()
    {
        recovering = false;
        recoveryVarPopped = false;
        recoveryFailed = false;
    }
    
    // Restores the state of the stack from the retained symbols and the last popped symbol
    private void restoreStack()
    {
        while (!retainStack.isEmpty())
        {
            stack.addFirst(retainStack.removeFirst());
        }
    }
    
    // Gets the follow set within the current context
    // This is derived from the first sets for the rules on the stack
    private Set<String> getFollowSet()
    {
        HashSet<String> follow = new HashSet<String>();
        follow.add(null);
        
        // Iterate over the current stack
        for (String s : stack)
        {
            if (table.isVariable(s))
            {
                // Add terminals from the current first set
                Set<String> first = table.getFirstSet(s);
                follow.addAll(first);
                
                // Only continue if the first set contains empty string
                if (!first.contains(null))
                {
                    follow.remove(null);
                    break;
                }
            }
            else
            {
                follow.remove(null);
                follow.add(s);
                break;
            }
        }
        
        return follow;
    }
    
    // Expands the symbol at the top of the stack as much as is possible without
    // making any assumptions (expands variables that only have one rule)
    private void expandSymbol()
    {
        //Implement this.
        // Consider removing repeated tokens
        // Restoring stack not working right? Skipping epsilons not working right?
        
        String top = stack.getFirst();
        
        while (table.isVariable(top))
        {
            String[] rule = table.getOnlyRule(top);
            if (rule == null)
            {
                // Multiple possible rules exist
                return;
            }
            else
            {
                stack.removeFirst();
                pushRule(rule);
            }
            
            top = stack.getFirst();
        }
    }
    
    // Expands variables at the top of the stack using only null productions
    private void expandNullProductions()
    {
        String top = stack.getFirst();
        // While there is a variable with a null production at the top that is not last
        while (stack.size() > 1 && table.isVariable(top) && table.getFirstSet(top).contains(null))
        {
            stack.removeFirst();
            top = stack.getFirst();
        }
    }
    
    private void performedStep(Deque<String> stack)
    {
        if (handler != null)
        {
            handler.performedDerivationStep(stack, tok.getConsumed());
        }
    }
    
    private void finished(int errorCount)
    {
        if (handler != null)
        {
            handler.parsingFinished(errorCount);
        }
    }
    
    private void encounteredError(Collection<String> expected, String encountered, int lineNo)
    {
        if (handler != null)
        {
            handler.parsingEncounteredError(expected, encountered, lineNo);
        }
    }
}