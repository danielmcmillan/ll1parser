/*
 * Tokeniser.java
 * Author: Daniel McMillan
*/

import java.util.*;
import java.io.*;
import java.util.regex.*;

/**
 * Class for reading through a file and extracting tokens.
 * The tokens are extracted based on a set of allowed tokens.
 * Characters that don't form part of an allowed token will be treated as a single token.
 */
public class Tokeniser
{
    // The set of tokens that may be extracted
    private Set<String> possibleTokens;
    
    // The queue of tokens ready to be consumed
    private ArrayDeque<String> tokens;
    
    // The BufferedReader for the file to read tokens from
    private BufferedReader reader;
    
    // The number of the line the token last read
    private int lineNo = 0;
    
    // StringBuilder for the tokens consumed so far
    private StringBuilder consumed;
    
    // The token currently being constructed from input
    private String currentToken = "";
    
    // The length of a token found within currentToken
    private int foundToken = 0;
    
   /**
    * Create a tokeniser for the file at the specified path
    */
    public Tokeniser(String filename, Set<String> tokens) throws FileNotFoundException
    {
        possibleTokens = tokens;
        
        this.tokens = new ArrayDeque<String>();
        
        reader = new BufferedReader(new FileReader(filename));
        
        consumed = new StringBuilder();
    }
    
    /**
     * Gets the next token from the file and consumes it
     * @return  The token that was read, or null if no tokens are left
    */
    public String consumeToken() throws IOException
    {
        makeTokenAvailable();
        
        if (tokens.isEmpty())
        {
            return null;
        }
        String token = tokens.removeFirst();
        consumed.append(token).append(" ");
        return token;
    }
    
    /**
     * Gets the next token from the file without consuming it
     * @return  The token that was read, or null if no tokens are left
    */
    public String peekToken() throws IOException
    {
        makeTokenAvailable();
        
        if (tokens.isEmpty())
        {
            return null;
        }
        return tokens.getFirst();
    }
    
    /**
     * Gets the line number of the last token
     * @return  The line number starting from 1
    */
    public int getLineNumber()
    {
        return lineNo;
    }
    
    /**
     * Gets the string of tokens consumed so far
     * @return  The string of tokens
    */
    public String getConsumed()
    {
        return consumed.toString();
    }
    
    // Reads tokens from the file if there are no tokens already available
    // If no more tokens are left, nothing will happen
    private void makeTokenAvailable() throws IOException
    {
        while (tokens.isEmpty())
        {
            if (!readLine())
            {
                break;
            }
        }
    }
    
    // Reads one line from the file if available and separates it into tokens
    private boolean readLine() throws IOException
    {
        String line = reader.readLine();
        
        if (line != null)
        {
            addTokens(line);
            ++lineNo;
            return true;
        }
        
        // Reached end of file, process the token in construction
        while (!currentToken.isEmpty())
        {
            processCurrentToken();
        }
        
        return false;
    }
    
    // Adds tokens to the queue from the given line of text
    private void addTokens(String line)
    {
        // Search for tokens in the line that exist in possibleTokens
        for (int i = 0; i < line.length(); ++i)
        {
            // Skip whitespace characters
            if (Character.isWhitespace(line.charAt(i)))
            {
                continue;
            }
            
            currentToken += line.charAt(i);
            
            // Check whether a token has been found
            if (possibleTokens.contains(currentToken))
            {
                // Remember the token that was found, but continue in case a larger token can be found
                foundToken = currentToken.length();
            }
            else
            {
                // Check whether a token can ever be reached from the current one
                boolean canFormToken = false;
                for (String t : possibleTokens)
                {
                    if (t.startsWith(currentToken))
                    {
                        // Possibly finding a token here, continue on
                        canFormToken = true;
                        break;
                    }
                }
                
                if (!canFormToken)
                {
                    // Can never form a token from currentToken
                    processCurrentToken();
                }
            }
        }
    }
    
    // Add the token found within the current one
    private void processCurrentToken()
    {
        if (foundToken == 0)
        {
            // No tokens found within currentToken, treat first character as token
            foundToken = 1;
        }
        
        tokens.addLast(currentToken.substring(0, foundToken));
        // Process the remaining part
        String remaining = currentToken.substring(foundToken);
        currentToken = "";
        foundToken = 0;
        addTokens(remaining);
    }
}