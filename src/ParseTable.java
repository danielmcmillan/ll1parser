/*
 * ParseTable.java
 * Author: Daniel McMillan
*/

import java.util.*;
import java.io.*;
import java.nio.file.*;

/**
 * Class for providing grammar rules deterministically, based on an LL(1) parse table
 */
public class ParseTable
{
    private static final String FIRST_RULE_SEPARATOR = "->";
    private static final String FOLLOW_RULE_SEPARATOR = "~>";
    
    // Mapping from variables to mappings from terminals to the list of symbols for a rule
    // A null terminal string represents the end of the string $ 
    private HashMap<String, HashMap<String, String[]>> table;
    private String startSymbol;
    private HashSet<String> terminals;
    private HashMap<String, HashSet<String>> firstSets;
    
    /**
    * Loads a parse table based on the definition in the specified file
    *
    * @param  file  the path to a file containing a parse table definition
    */
    public ParseTable(String file)
        throws IOException, InvalidParseTableException
    {
        loadFromFile(file);
    }
    
    /**
    * Get the variable symbol to begin with when parsing
    */
    public String getStartSymbol()
    {
        return startSymbol;
    }
    
    /**
    * Gets the symbols for the rule that can be used to derive the given terminal
    *
    * @param  variable  the variable that the terminal needs to be derived from
    * @param  terminal  the terminal that needs to be derived, or null for end of string
    *
    * @return           an array of symbols for the rule deriving the required terminal
    */
    public String[] getRule(String variable, String terminal)
    {
        // Check that the given variable is valid
        if (!table.containsKey(variable))
        {
            throw new IllegalArgumentException(
                String.format("The variable %s is not in the table", variable));
        }
        
        HashMap<String, String[]> row = table.get(variable);
        
        // Check whether there is a rule defined for the given terminal
        if (!row.containsKey(terminal))
        {
            // There is no way to generate the given terminal
            return null; 
        }
        
        return row.get(terminal);
    }
    
    /**
    * Gets the symbols for the rule of the given variable if only one rule exists
    *
    * @param  variable  the variable to get the rule for
    *
    * @return           an array of symbols for the rule, or null if multiple rules exist
    */
    public String[] getOnlyRule(String variable)
    {
        // Check that the given variable is valid
        if (!table.containsKey(variable))
        {
            throw new IllegalArgumentException(
                String.format("The variable %s is not in the table", variable));
        }
        
        HashMap<String, String[]> row = table.get(variable);
        
        String[] rule = null;
        
        for (String[] r : row.values())
        {
            if (rule == null)
            {
                rule = r;
            }
            else if (!ruleEqual(rule, r))
            {
                // Multiple different rules exist
                return null;
            }
        }
        
        return rule;
    }
    
    // Checks whether the given rules are equal
    private boolean ruleEqual(String[] a, String[] b)
    {
        if (a.length != b.length)
        {
            return false;
        }
        
        // Compare each symbol in the rules 
        for (int i = 0; i < a.length; ++i)
        {
            if (!a[i].equals(b[i]))
            {
                return false;
            }
        }
        
        return true;
    }
    
    /**
    * Gets the set of all terminals that can be derived
    *
    * @return   a set of terminals that can be derived
    */
    public Set<String> getAllTerminals()
    {
        return terminals;
    }
    
    /**
    * Checks whether a symbol is a variable
    *
    * @param symbol     the symbol to check
    * @return           whether the specified symbol is a variable
    */
    public boolean isVariable(String symbol)
    {
        return table.containsKey(symbol);
    }
    
    /**
    * Gets the first set for the specified variable
    *
    * @param variable   the variable
    * @return           the first set
    */
    public Set<String> getFirstSet(String variable)
    {
        // Check that the given variable is valid
        if (!firstSets.containsKey(variable))
        {
            throw new IllegalArgumentException(
                String.format("The variable %s is not in the table", variable));
        }
        
        return firstSets.get(variable);
    }
    
    // Initialises this ParseTable by loading a definition from the specified file
    private void loadFromFile(String file)
        throws InvalidPathException, IOException, InvalidParseTableException
    {
        // Read the file into a list of strings for each line
        List<String> lines = Files.readAllLines(Paths.get(file));
        
        table = new HashMap<String, HashMap<String, String[]>>();
        
        // Read the start variable from the first line
        startSymbol = lines.get(0);
        
        // To get the set of terminals, initially all symbols are added to the set
        terminals = new HashSet<String>();
        
        firstSets = new HashMap<String, HashSet<String>>();
        
        // Read the table cells from each line
        for (int i = 1; i < lines.size(); ++i)
        {
            loadCellDefinition(lines.get(i), i + 1);
        }
        
        // Check that the start symbol was defined to be a variable
        if (!table.containsKey(startSymbol))
        {
            throw new InvalidParseTableException("start symbol not a variable");
        }
        
        // Remove the variables from the terminals set
        terminals.removeAll(table.keySet());
    }
    
    // Loads the specified table cell definition into this ParseTable
    private void loadCellDefinition(String definition, int lineNo)
        throws InvalidParseTableException
    {
        // Skip the line if it is whitespace only
        String def = definition.trim();
        if (def.isEmpty())
        {
            return;
        }
        
        // Split the definition into whitespace separated parts
        String[] parts = def.split("\\s+");

        // Check that there are enough parts
        if (parts.length < 2)
        {
            throw new InvalidParseTableException("not enough elements at line " + lineNo);
        }
        
        String variable = parts[0];
        HashMap<String, String[]> row = addOrGetRow(variable);
        
        String terminal = parts[1];
        
        // The index following the FILE_SEPARATOR
        int ruleStartIndex = 3;
        
        // If no terminal was given, treat as end of file, represented by null string
        if (isSeparator(terminal))
        {
            terminal = null;
            ruleStartIndex = 2;
        }
        // If a terminal was given, we expect the next part to be separator
        else if (parts.length < 3 || !isSeparator(parts[2]))
        {
            throw new InvalidParseTableException("no rule defined at line " + lineNo);
        }
        
        // Allow separators to be escaped using backslash
        if (terminal != null)
        {
            if (terminal.equals("\\" + FIRST_RULE_SEPARATOR))
            {
                terminal = FIRST_RULE_SEPARATOR;
            }
            else if (terminal.equals("\\" + FOLLOW_RULE_SEPARATOR))
            {
                terminal = FOLLOW_RULE_SEPARATOR;
            }
        }
        
        // Check that a rule was not already defined for this cell
        if (row.containsKey(terminal))
        {
            throw new InvalidParseTableException(String.format(
                "line %d would make parsing non-deterministic: rule for variable %s and " +
                "terminal %s already defined", lineNo, variable, terminal));
        }
        
        // If -> separator is used, terminal must be in the first set
        if (parts[ruleStartIndex - 1].equals(FIRST_RULE_SEPARATOR))
        {
            addToFirstSet(variable, terminal);
        }
        else
        {
            // Variable has a rule derived from a follow set,
            // and therefore has empty string in its first set
            addToFirstSet(variable, null);
        }
        
        String rule[];
        
        // Check whether the rule is an empty string
        if (parts.length <= ruleStartIndex)
        {
            // Represent null production by empty array
            rule = new String[0];
        }
        else
        {
            // Create an array of the symbols for the rule
            rule = Arrays.copyOfRange(parts, 3, parts.length);
        }
        
        // Add each symbol from the rule to the terminals set (including variables)
        for (String s : rule)
        {
            terminals.add(s);
        }
        
        // Add the rule to the cell
        row.put(terminal, rule);
    }
    
    // Get the row of the parse table for this variable, adding it if required
    private HashMap<String, String[]> addOrGetRow(String variable)
    {
        if (table.containsKey(variable))
        {
            return table.get(variable);
        }
        
        HashMap<String, String[]> row = new HashMap<String, String[]>();
        table.put(variable, row);
        return row;
    }
    
    // Adds the specified terminal to the first set of the specified variable
    private void addToFirstSet(String variable, String terminal)
    {
        if (!firstSets.containsKey(variable))
        {
            firstSets.put(variable, new HashSet<String>());
        }
        firstSets.get(variable).add(terminal);
    }
    
    // Checks whether the specified string is a separator for ParseTable definition file
    private boolean isSeparator(String part)
    {
        return part.equals(FIRST_RULE_SEPARATOR) || part.equals(FOLLOW_RULE_SEPARATOR);
    }
    
    // Get a string representation of the parse table
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        
        for (String var : table.keySet())
        {
            HashMap<String, String[]> row = table.get(var);
            
            for (String term : row.keySet())
            {
                sb.append(String.format("%s %s -> ", var, (term == null ? "$" : term)));
                for (String symb : row.get(term))
                {
                    sb.append(String.format("%s ", symb));
                }
                sb.append("\n");
            }
            sb.append("\n");
        }
        
        return sb.toString();
    }
}