/*
 * ParseEventHandler.java
 * Author: Daniel McMillan
*/

import java.util.*;

/**
 * An interface for a class that can handle events relating to parsing text
*/
public interface ParseEventHandler
{
    /**
     * Called when parsing is complete
     *
     * @param errorCount  The number of errors encountered while parsing
    */
    public void parsingFinished(int errorCount);
    
    /**
     * Called when a derivation step is performed by a parser
     *
     * @param stack     The state of the stack after performing the derivation
     * @param consumed  The string of tokens that have been parsed so far
    */
    public void performedDerivationStep(Deque<String> stack, String consumed);
    
    /**
     * Called when an error is encountered while parsing a string
     *
     * @param expected      The token that was expected when the error occured,
     *                      or null if the error results from an extraneous token
     * @param encountered   The token that was found when the error occured,
     *                      or null if the error results from a missing token 
     * @param encountered   The line number in the input where the error was found
    */
    public void parsingEncounteredError(Collection<String> expected, String encountered, int lineNo);
}