/*
 *  MyProgram.java
 *  Author: Daniel McMillan
 *  Unikey: dmcm0887
 *  SID: 450398425
 *
 *  Program to parse the contents of a file using a parse table
 *  Arguments: file_to_parse [-e] [-t parse_table_file]
 *  If no parse table is specified, the default file 'parsetable' is used
 *  -e enables error recovery, -o specifies derivation step output should be omitted
*/

import java.util.*;
import java.io.*;

public class MyProgram implements ParseEventHandler
{
    // The default parse table file
    static final String DEFAULT_PARSE_TABLE = "parsetable";
    // The string to use to represent end of file
    static final String EOF = "EOF";
    // The width in characters used to display the consumed part of string 
    static final int CONSUMED_WIDTH = 40;
    
    static boolean enableErrorRecovery = false;
    static boolean outputDerivationSteps = true;
    
    public static void main(String[] args)
    {
        // The filename of the file to parse
        String fileToParse = null;
        // Whether the last argument read was -t
        boolean expectingParseTable = false;
        // The filename of the parse table to use
        String parseTable = null;
        
        // Read through the arguments
        for (String arg : args)
        {
            if (expectingParseTable)
            {
                if (parseTable != null)
                {
                    System.out.printf("Only one parse table can be specified\n", arg);
                    return;
                }
                expectingParseTable = false;
                parseTable = arg;
            }
            else if (arg.startsWith("-"))
            {
                // Interpret the option
                if (arg.equals("-e"))
                {
                    enableErrorRecovery = true;
                }
                else if (arg.equals("-t"))
                {
                    expectingParseTable = true;
                }
                else if (arg.equals("-o"))
                {
                    outputDerivationSteps = false;
                }
                else
                {
                    System.out.printf("Unknown option \'%s\'\n", arg);
                    return;
                }
            }
            else
            {
                // Expect the file to parse
                if (fileToParse != null)
                {
                    System.out.printf("Only one file to parse can be specified\n", arg);
                    return;
                }
                fileToParse = arg;
            }
        }
        
        // Check a parse table is not still expected
        if (expectingParseTable)
        {
            System.out.println("Expected parse table file name");
            return;
        }
        
        // Check that a file to parse was specified
        if (fileToParse == null)
        {
            System.out.println("No file to parse was specified");
            return;
        }
        
        // Set the default parse table if none was specified
        if (parseTable == null)
        {
            parseTable = DEFAULT_PARSE_TABLE;
        }
        
        parseFile(fileToParse, parseTable, enableErrorRecovery);
    }
    
    // Parse the file, printing derivation steps and error messages
    private static void parseFile(String file, String parseTableFile, boolean enableErrorRecovery)
    {
        try
        {
            ParseTable table = new ParseTable(parseTableFile);
            
            Parser p = new Parser(table, file, enableErrorRecovery);
            p.setEventHandler(new MyProgram());
            p.parse();
        }
        catch (IOException e)
        {
            System.out.println("Error reading file: " + e.getMessage());
        }
        catch (InvalidParseTableException e)
        {
            System.out.println("Invalid parse table definition: " + e.getMessage());
        }
    }
    
    public void parsingFinished(int errorCount)
    {
        if (errorCount == 0)
        {
            System.out.println("ACCEPTED");
        }
        else if (enableErrorRecovery)
        {
            // Show number of errors if error recovery is enables
            System.out.printf("REJECTED (%d errors)\n", errorCount);
        }
        else
        {
            System.out.println("REJECTED");
        }
    }
    
    public void performedDerivationStep(Deque<String> stack, String consumed)
    {
        if (!outputDerivationSteps)
        {
            return;
        }
        
        // Print the consumed part padded/truncates to CONSUMED_WIDTH, followed by stack contents
        System.out.printf("%" + CONSUMED_WIDTH + "s [ %s ]\n",
            truncateStart(consumed, CONSUMED_WIDTH), joinCollection(stack, " ", " ", ""));
    } 
    
    public void parsingEncounteredError(Collection<String> expected, String encountered, int lineNo)
    {
        System.out.printf("\nError at line %d: ", lineNo);
        if (expected == null)
        {
            System.out.printf("expected %s instead of %s\n", EOF, encountered);
        }
        else if (encountered == null)
        {
            System.out.printf("reached %s while expecting %s\n", EOF,
                joinCollection(expected, ", ", " or ", EOF));
        }
        else
        {
            System.out.printf("expected %s instead of %s\n",
                joinCollection(expected, ", ", " or ", EOF), encountered);
        }
        System.out.println();
    }
    
    // Joins the collection into a string, separating elements with the given separators
    // null strings will be replaced by def
    private static String joinCollection(Collection<String> collection,
        String separator, String finalSeparator, String def)
    {
        // Replace null with the default
        if (collection.contains(null))
        {
            collection.remove(null);
            collection.add(def);
        }
        
        Iterator<String> i = collection.iterator();
        
        if (!i.hasNext())
        {
            return def;
        }
        
        StringBuilder sb = new StringBuilder();
        sb.append(i.next());
        
        while (i.hasNext())
        {
            String next = i.next();
            // Check whether this is the final element
            if (i.hasNext())
            {
                sb.append(separator);
            }
            else
            {
                sb.append(finalSeparator);
            }
            sb.append(next);
        }
        
        return sb.toString();
    }
    
    // Truncates string by removing from start and adding ...
    private static String truncateStart(String s, int length)
    {
        if (s.length() <= length)
        {
            return s;
        }
        
        while (s.length() + 5 > length)
        {
            s = s.substring(s.indexOf(" ") + 1);
        }
        return String.format("%-" + (length - s.length()) + "s", " ...") + s;
    }
}